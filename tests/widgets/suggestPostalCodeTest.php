<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/suggestLineEditTest.php';

class Widget_SuggestPostalCodeTest extends Widget_SuggestLineEditTest
{
    protected $itemClass = 'Widget_SuggestPostalCode';
}
