<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/inputWidgetTest.php';

class Widget_DatePickerTest extends Widget_InputWidgetTest
{
    protected $itemClass = 'Widget_DatePicker';
}
