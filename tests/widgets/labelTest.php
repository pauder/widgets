<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/widgetTest.php';

class Widget_LabelTest extends Widget_WidgetTest
{
    protected $itemClass = 'Widget_Label';
    
    
    /**
     * A label with colon() should have an html end with a
     */
    public function testLabelWithColon()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $item->colon();
        $item->setText('My label');

        $W = bab_Widgets();
        
        $translatedColon = widget_translate(':');
        
        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);
        
        $this->assertContains(
            $translatedColon,
            $html
        );
    }
    
    
    public function testLabelWithColonWhenColonAlreadyPresent()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
    
        $translatedColon = widget_translate(':');
    
        $item->colon();
        $item->setText('My title' . $translatedColon);
    
        $W = bab_Widgets();
    
    
        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);
    
        $this->assertNotContains(
            $translatedColon . $translatedColon,
            $html
        );
    }
}


