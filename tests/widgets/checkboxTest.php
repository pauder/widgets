<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/inputWidgetTest.php';

class Widget_CheckBoxTest extends Widget_InputWidgetTest
{
    protected $itemClass = 'Widget_CheckBox';


    /**
     * The html string returned by the display() method must contain the inputWidget name.
     */
    public function testSimpleNameIsPresentInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $name = 'myName';
        $item->setName($name);

        $W = bab_Widgets();
        $canvas = $W->HtmlCanvas();

        $html = $item->display($canvas);

        $xpathQueryResult = $this->getXPathMatchAttribute($html, 'name', $this->getHtmlName($name));

        $this->assertEquals( 2, $xpathQueryResult->length, 'There were no name attribute matching the item name for ' . $this->itemClass );
    }


    /**
     * The html string returned by the display() method must contain the inputWidget name.
     */
    public function testMultipleNameIsPresentInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $name = array('myContact', 'name');
        $item->setName($name);

        $W = bab_Widgets();
        $canvas = $W->HtmlCanvas();

        $html = $item->display($canvas);
        $xpathQueryResult = $this->getXPathSearchAttribute($html, 'name', $this->getHtmlName($name));
        $this->assertEquals( 2, $xpathQueryResult->length, 'There were no name attribute matching the full item name for ' . $this->itemClass );
    }
}
