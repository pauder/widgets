## Interfaces utilisateurs ##

[![Build Status](https://drone.io/bitbucket.org/cantico/widgets/status.png)](https://drone.io/bitbucket.org/cantico/widgets/latest)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/cantico/widgets/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/cantico/widgets/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/cantico/widgets/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/cantico/widgets/?branch=master)

[Documentation](http://wiki.ovidentia.org/index.php/Widgets)

Widgets, bibliothèque d'éléments de l'interface utilisateur