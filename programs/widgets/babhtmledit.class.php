<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/inputwidget.class.php';




/**
 * A Widget_BabHtmlEdit is a widget that let the user enter multiple lines of text using the
 * ovidentia Html editor.
 */
class Widget_BabHtmlEdit extends Widget_InputWidget implements Widget_Displayable_Interface
{
    private $_nbColumns = 80;

    private $_nbLines = 5;

    private $_email = false;



    /**
     * Sets the visible width of the text edit.
     *
     * @param int $nbColumns    The number of visible columns.
     * @return self
     */
    public function setColumns($nbColumns)
    {
        assert('is_int($nbColumns); /* The "nbColumns" parameter must be an integer */');
        $this->_nbColumns = $nbColumns;
        return $this;
    }


    /**
     * Enable email format
     *
     * @param bool $enable
     * @return self
     */
    public function setEmail($enable = true)
    {
        $this->_email = $enable;

        return $this;
    }


    /**
     * Returns the visible width (in characters) of the text edit.
     */
    public function getColumns()
    {
        return $this->_nbColumns;
    }


    /**
     * Sets the vertical input size (in characters) of the text edit.
     *
     * @param int $nbLines  The number of visible lines.
     * @return self
     */
    public function setLines($nbLines)
    {
        assert('is_int($nbLines); /* The "nbLines" parameter must be an integer */');
        $this->_nbLines = $nbLines;
        return $this;
    }


    /**
     * Returns the vertical input size (in characters) of the text edit.
     *
     * @return int
     */
    public function getLines()
    {
        return $this->_nbLines;
    }


    /**
     * {@inheritDoc}
     * @see Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-babhtmledit';
        return $classes;
    }


    /**
     * Get html content for saving in database from the POST
     *
     * @throws ErrorException
     *
     * @param array $fullname
     * @return string
     */
    public function getContent(Array $fullname = null)
    {
        if (null === $fullname) {
            $fullname = $this->getFullName();
        }

        $lastName = array_pop($fullname);
        $currentArray = $_POST;
        foreach ($fullname as $name) {
            if (! isset($currentArray[$name])) {
                throw new ErrorException('The name %s is not defined in _POST');
            }
            $currentArray = $currentArray[$name];
        }

        $html = $currentArray[$lastName];
        bab_editor_record($html);

        return $html;
    }


    /**
     * {@inheritdoc}
     *
     * @see Widget_Item::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        include_once $GLOBALS['babInstallPath'] . 'utilit/editorincl.php';
        $editor = new bab_contentEditor($this->getId());
        $editor->setRequestFieldName($canvas->getHtmlName($this->getFullName()));
        $editor->setContent($this->getValue());
        if ($this->_email) {
            $editor->setFormat('email');
        } else {
            $editor->setFormat('html');
        }

        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';

            return $canvas->div($this->getId(), $classes, array(
                $editor->getHtml()
            ), $this->getCanvasOptions());
        }

        $editor->setParameters(array(
            'height' => $this->getLines() * 20
        ));
        $html = $editor->getEditor();

        $html .= $canvas->loadScript($this->getId(), $GLOBALS['babInstallPath'] . 'scripts/bab_dialog.js');
        $html .= $canvas->loadScript($this->getId(), $GLOBALS['babInstallPath'] . 'scripts/text_toolbar.js');

        return $html;
    }
}
