<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/tellineedit.class.php';


/**
 * Constructs a Widget_InternationalTelLineEdit.
 *
 * @param string        $id         The item unique id.
 * @return Widget_InternationalTelLineEdit
 */
function Widget_InternationalTelLineEdit($id = null)
{
    return new Widget_InternationalTelLineEdit($id);
}


/**
 * A Widget_InternationalTelLineEdit is a widget that lets the user enter a telephone number, formatted by country.
 */
class Widget_InternationalTelLineEdit extends Widget_TelLineEdit implements Widget_Displayable_Interface
{

    public function getClasses()
    {
        $classes = Widget_LineEdit::getClasses();
        $classes[] = 'widget-internationaltellineedit';

        if (isset($this->type)) {
            $classes[] = 'widget-internationaltellineedit-'.$this->type;
        }

        return $classes;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_TelLineEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if (!$this->isMandatory()) {
            $message = $this->getMetadata('submitMessage');
            $message .= " \n".widget_translate('Do you want to submit the form anyway?');
            $this->setSubmitMessage($message);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return parent::display($canvas)
            . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.internationaltellineedit.jquery.js')
            . $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.internationaltellineedit.css');
    }
    
    
    /**
     * Sets the countries to be displayed.
     * @param array $countries
     *     Array of iso country code
     * @return $this
     */
    public function setCountries($countries)
    {
        assert('is_array($countries); /* The "countries" parameter must be an array */');
        $this->setMetadata('onlyCountries', $countries);
        return $this;
    }
    
    /**
     * Specify the countries to appear at the top of the list.
     * @param array $countries
     *     Array of iso country code
     * @return $this
     */
    public function setPreferredCountries($countries)
    {
        assert('is_array($countries); /* The "countries" parameter must be an array */');
        $this->setMetadata('preferredCountries', $countries);
        return $this;
    }
    
    /**
     * Set the initial country selection by specifying its country code.
     * @param array $country
     *     Iso country code
     * @return $this
     */
    public function setInitialCountry($coutry)
    {
        assert('is_string($coutry); /* The "coutry" parameter must be a string */');
        $this->setMetadata('initialCountry', $coutry);
        return $this;
    }
    
    /**
     * Return the country code array of displayed countries.
     * @return array
     */
    public function getCountries()
    {
        return $this->getMetadata('onlyCountries');
    }
    
    /**
     * Return the country code array of preferred countries.
     * @return array
     */
    public function getPreferredCountries()
    {
        return $this->getMetadata('preferredCountries');
    }
    
    /**
     * Return the country code of inital country.
     * @return array
     */
    public function getInitialCountry()
    {
        return $this->getMetadata('initialCountry');
    }
}
