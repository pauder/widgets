<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_NamedContainer.
 *
 * @param	string		$name		The item name
 * @param 	string		$id			The item unique id.
 * @return Widget_Frame
 */
function Widget_NamedContainer($name, $id = null)
{
	return new Widget_NamedContainer($name, $id);
}



/**
 * A Widget_NamedContainer is a container with no decoration that only display its 
 * associated layout but that carries a name and optional metadata
 *
 */
class Widget_NamedContainer extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

	/**
	 * @param	string		$name		The item name
	 * @param 	string 		$id			The item unique id.
	 * @return Widget_Frame
	 */
	public function __construct($name = null, $id = null)
	{
		
		require_once FUNC_WIDGETS_PHP_PATH . 'layout.class.php';
		$layout = new Widget_Layout();
		

		parent::__construct($id, $layout);
		
		if (isset($name)) {
			$this->setName($name);	
		}
	}

	public function addClass($className)
	{
		$this->getLayout()->addClass($className);
		return $this;
	}

	public function display(Widget_Canvas $canvas)
	{
		$html = $this->getLayout()->display($canvas);
		$html .= $canvas->metadata($this->getId(), $this->getMetadata());

		return $html;
	}

}

