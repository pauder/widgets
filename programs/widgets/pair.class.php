<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once FUNC_WIDGETS_PHP_PATH . 'widget.class.php';

/**
 * Constructs a Widget_Pair.
 * @param string		$id			The item unique id.
 * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
 * @return Widget_Pair
 */
function Widget_Pair($id = null, Widget_Layout $layout = null)
{
	return new Widget_Pair($id);
}



class Widget_Pair extends Widget_ContainerWidget implements Widget_Displayable_Interface 
{
	private	$first;
	private	$second;
	
	/**
	 * @param string		$id			The item unique id.
	 * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
	 * @return Widget_Pair
	 */
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		if (null === $layout) {
			require_once FUNC_WIDGETS_PHP_PATH . 'layout.class.php';
			$layout = new Widget_Layout();
		}
		
		
		parent::__construct($id);
		
		$this->setLayout($layout);
	}
	
	
	public function addItem() {
		trigger_error('On the Pair widget, use methods setFirst($w) and setSecond($w)');
	}
	
	
	public function setFirst($w) {
		$this->first = $w;
		parent::addItem($w);
		
		return $this;
	}
	
	public function getFirst() {
		return $this->first;
	}
	
	public function setSecond($w) {
		$this->second = $w;
		parent::addItem($w);
		return $this;
	}
	
	public function getSecond() {
		return $this->second;
	}
	
	
	/**
	 * The item classes.
	 *
	 * @return array
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-pair';
		return $classes;
	}

	
	public function display(Widget_Canvas $canvas)
	{
		return $canvas->div(
			$this->getId(),
			$this->getClasses(),
			array(
				$canvas->div('', array('first'), array($this->getFirst())),
				$canvas->div('', array('second'), array($this->getSecond()))
			)
		);
	}
}
