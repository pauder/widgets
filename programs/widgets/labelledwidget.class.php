<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/widget.class.php';





class Widget_LabelledWidget extends Widget_Widget implements Widget_Displayable_Interface
{
	private $labelText = '';
	private $label = null;
	private $inputWidget = null;
	private $balloon = null;

	/**
	 * @var bool
	 */
	private $descriptionBelowLabel = false;


	/**
	 * @var string
	 */
	private $description = null;

    /**
     * @var Widget_Layout
     */
	private $layout = null;

	private $fieldSuffix = null;

	private $tmpName = null;


	/**
	 *
	 * @param string                       	$labelText
	 * @param Widget_Displayable_Interface 	$inputWidget
	 * @param string                       	$id
	 * @param string						$suffix		 suffix for input field, example : unit
	 */
	public function __construct($labelText = '', Widget_Displayable_Interface $inputWidget = null, $id = null, $suffix = null, $balloon = null)
	{
		$this->labelText = $labelText;
		$this->inputWidget = $inputWidget;
		$this->fieldSuffix = $suffix;
		$this->balloon = $balloon;

		$this->layout = $this->createFieldLayout();
		parent::__construct($id);
	}


	/**
	 *
	 * @param bool $mandatory
	 * @param string $string
	 * @return self
	 */
	public function setInputMandatory($mandatory, $string)
	{
		$this->inputWidget->setMandatory($mandatory, $string);
		return $this;
	}


	/**
	 * @return Widget_Layout
	 */
	private function createFieldLayout()
	{
		$W = bab_Widgets();

		$this->label = $W->Label($this->labelText)
			->addClass('widget-description');

		$inputWidget = $this->inputWidget;

		$balloon = null;
		if ($this->balloon) {
			$inputWidget = $W->FlowItems($inputWidget)->setTitle($this->balloon);
			$balloon = $W->Balloon()->setCanvasOptions($this->inputWidget->Options()->width(13,'em'))->displayOnMouseHover($inputWidget)->addItem($W->Label($this->balloon));
		}

		if ($this->inputWidget instanceof Widget_AssociableLabel_Interface) {
			$this->label->setAssociatedWidget($this->inputWidget);
		}


		if ($this->inputWidget instanceof Widget_CheckBox || $this->inputWidget instanceof Widget_Radio) {

			$layout = $W->HBoxLayout()
				->setHorizontalSpacing(0.2, 'em')
				->addItem($inputWidget);

		} else {
			$layout = $W->VBoxLayout()
				->setVerticalSpacing(0.2, 'em')
				->addItem($this->label);

			if (isset($this->fieldSuffix)) {
				$layout->addItem(
				    $W->Items(
				        $inputWidget->addClass('form-control'),
				        $W->Label($this->fieldSuffix)->setSizePolicy('input-group-addon')
				    )
				    ->addClass('input-group')
				);
			} else {
				$layout->addItem($inputWidget);
			}
		}
		$layout->addItem($balloon);

		return $layout;
	}


	/**
	 * @return self
	 */
	public function setParent($parent)
	{
		$this->layout->setParent($parent);
		return $this;
	}

	public function getParent()
	{
		return $this->layout->getParent();
	}

	/**
	 *
	 * @return string
	 */
	public function getLabelText()
	{
		return $this->labelText;
	}

	/**
	 *
	 * @param mixed $value
	 * @return self
	 */
	public function setValue($value)
	{
		if (isset($this->inputWidget)) {
			$this->inputWidget->setValue($value);
		}
		return $this;
	}

	/**
	 *
	 * @param mixed $value
	 * @return self
	 */
	public function setDisplayMode()
	{
		$this->inputWidget->setDisplayMode();
		return $this;
	}

	/**
     * Set a widget do be displayed only if the value of the input widget match the ID of the widget or one of values set in the second parameter
     * this can be called for more than one widget
     *
     * @param 	Widget_Displayable_Interface 	$displayable
     * @param	array							$values
     * @param	boolean							$displayMode if false it will hide element if values parameter match the field value
     *
     * @return Widget_InputWidget
	 */
	public function setAssociatedDisplayable(Widget_Displayable_Interface $displayable, Array $values = null, $displayMode = true)
	{
		$this->inputWidget->setAssociatedDisplayable($displayable, $values, $displayMode);
		return $this;
	}

	/**
	 *
	 * @return mixed;
	 */
	public function getValue()
	{
		if (isset($this->inputWidget)) {
			return $this->inputWidget->getValue();
		}
		return null;
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Widget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-labelled-widget';

		return $classes;
	}


	public function testMandatory()
	{
		if (isset($this->inputWidget)) {
			return $this->inputWidget->testMandatory();
		}
		return parent::testMandatory();
	}


	/**
	 * Calls the associated widget setName() method if available.
	 *
	 * @param string $name
	 * @return Widget_LabelledWidget
	 */
	public function setName($name)
	{
		if (isset($this->inputWidget)) {
			$methodSetName = array($this->inputWidget, 'setName');
			if (is_callable($methodSetName, true)) {
				$this->inputWidget->setName($name);
			}
		} else {
		    $this->tmpName = $name;
		}
		return $this;
	}



	/**
	 * Calls the associated widget getName() method if available.
	 *
	 * @return string|array
	 */
	public function getName()
	{
		if (isset($this->inputWidget)) {
		    $methodGetName = array($this->inputWidget, 'getName');
		    if (is_callable($methodGetName, true)) {
		        return $this->inputWidget->getName();
		    }
		}
		return null;
	}



	public function setInputWidget(Widget_InputWidget $widget)
	{
	    $this->inputWidget = $widget;
	    if (isset($this->tmpName)) {
	        $this->inputWidget->setName($this->tmpName);
	        $this->tmpName = null;
	    }

	    return $this;
	}


	/**
	 *
	 *
	 * @return Widget_Label
	 */
	public function getLabelWidget()
	{
	    return $this->label;
	}


	/**
	 *
	 *
	 * @return Widget_InputWidget
	 */
	public function getInputWidget()
	{
		return $this->inputWidget;
	}


	/**
	 *
	 * @return Widget_LabelledWidget
	 */
	public function colon($active = true)
	{
		if (isset($this->label)) {
			$this->label->colon($active);
		}
		return $this;
	}


	/**
	 * @param bool $below
	 * @return self
	 */
	public function setDescriptionBelowLabel($below = true)
	{
	    $this->descriptionBelowLabel = $below;
	    return $this;
	}

	/**
	 * @return bool
	 */
	public function getDescriptionBelowLabel()
	{
	    return $this->descriptionBelowLabel;
	}

	/**
	 *
	 * @param Widget_Canvas $canvas
	 * @return string
	 */
	public function display(Widget_Canvas $canvas)
	{
		$W = bab_Widgets();

		if (isset($this->description)) {
			$descriptionLabel = $W->Label($this->description)->addClass('widget-long-description');

			if ($this->inputWidget instanceof Widget_CheckBox || $this->inputWidget instanceof Widget_Radio) {

				$descriptionLayout = $W->VBoxItems(
					$this->label->colon(false),
					$descriptionLabel
				)
				->setVerticalAlign('middle')
				->setHorizontalSpacing(0.2, 'em');

				$this->layout->addItem($descriptionLayout);

			} else {

			    if ($this->descriptionBelowLabel) {
			        $this->layout->addItem($descriptionLabel, 1);
			    } else {
			        $this->layout->addItem($descriptionLabel);
			    }


			}

		} else {
			if ($this->inputWidget instanceof Widget_CheckBox || $this->inputWidget instanceof Widget_Radio) {
				$this->layout->addItem($this->label->colon(false))->setVerticalAlign('middle');
			}
		}

		return $canvas->div(
		    $this->getId(),
		    $this->getClasses(),
		    array($this->layout->display($canvas)),
		    $this->getCanvasOptions(),
		    $this->getTitle(),
		    $this->getAttributes()
		).$canvas->metadata($this->getId(), $this->getMetadata());
	}



	/**
	 *
	 * @param string $description
	 *
	 * @return self
	 */
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}




	/**
	 * Replace item by another
	 *
	 * @param string $oldId
	 * @param Widget_Displayable_Interface $item
	 * @return bool
	 */
	public function replaceItem($oldId, Widget_Displayable_Interface $item)
	{

		if ($this->inputWidget->getId() === $oldId) {
			$this->inputWidget = $item;

			$this->layout = $this->createFieldLayout();
			return true;
		}

		return false;
	}
}
