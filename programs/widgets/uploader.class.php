<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/inputwidget.class.php';



/**
 * Constructs a Widget_Uploader.
 *
 * @param string        $id         The item unique id.
 * @return Widget_Uploader
 */
function Widget_Uploader($id = null)
{
    return new Widget_Uploader($id);
}


/**
 * A Widget_Uploader is a widget that let the user select files for uploading to the server.
 */
class Widget_Uploader extends Widget_InputWidget implements Widget_Displayable_Interface
{
    /**
     * @var string[]
     */
    private $acceptedMimeTypes = array();

    /**
     * @var int
     */
    private $_size;

    /**
     *
     * @var bool
     */
    private $multiple = false;

    /**
     * @var Widget_Item
     */
    private $imagePreview = null;

    /**
     * @param string $id            The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }


    /**
     * Defines if the uploader will allow multiple file uploads at once.
     *
     * @param string $status
     * @return self
     */
    public function setMultiple($status = true)
    {
        $this->multiple = $status;
        return $this;
    }

    /**
     * Sets the visible size of the uploader.
     *
     * @param int $size
     * @return self
     */
    public function setSize($size)
    {
        assert('is_int($size); /* The "size" parameter must be an integer */');
        $this->_size = $size;
        return $this;
    }

    /**
     * Returns the visible size (in characters) of the uploader.
     *
     * @return int
     */
    public function getSize()
    {
        return $this->_size;
    }


    /**
     * Sets the array of accepted mime types.
     *
     * @param array|string $mimeTypes   array of mimetypes or comma-separated string or mimetypes.
     * @return self
     */
    public function setAcceptedMimeTypes($mimeTypes)
    {
        if (is_string($mimeTypes)) {
            $this->acceptedMimeTypes = explode(',', $mimeTypes);
        } else {
            $this->acceptedMimeTypes = $mimeTypes;
        }
        return $this;
    }


    /**
     * Returns the array of accepted mime types.
     *
     * @return string[]
     */
    public function getAcceptedMimeTypes()
    {
        return $this->acceptedMimeTypes;
    }



    /**
     * Sets a widget (having a setImageUrl method) that will display an preview of the file to upload.
     *
     * @param Widget_Item $image
     * @return self
     */
    public function setPreviewImageItem($image)
    {
        $this->imagePreview = $image;
        return $this;
    }



    /**
     * @see Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-uploader';

        if ($this->isDisplayMode()) {
            $classes[] = 'widget-displaymode';
        }

        return $classes;
    }


    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_Displayable_Interface#display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if (isset($this->imagePreview)) {
            $this->setMetadata('imagePreview', $this->imagePreview->getId());
        }
        if ($this->isDisplayMode()) {
            return $canvas->span(
                $this->getId(),
                $this->getClasses(),
                array()
            );
        }

        return $canvas->fileUpload(
            $this->getId(),
            $this->getClasses(),
            $this->getFullName(),
            $this->getAcceptedMimeTypes(),
            $this->getSize(),
            $this->multiple,
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata());
    }
}
