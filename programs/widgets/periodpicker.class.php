<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';


/**
 * Constructs a Widget_PeriodPicker.
 *
 * @param string		$id			The item unique id.
 * @param bool	 		$withTime	If the periode is selected with time.
 * @return Widget_DatePicker
 */
function Widget_PeriodPicker($id = null, $withTime = false)
{
    return new Widget_DatePicker($id, $withTime);
}


/**
 * A Widget_PeriodPicker is a widget that let the user enter a period of time (2 dates).
 */
class Widget_PeriodPicker extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

    private $from = null;
    private $to = null;
    private $fromLabel = null;
    private $toLabel = null;

    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null, $withTime = false)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'flowlayout.class.php';
        require_once FUNC_WIDGETS_PHP_PATH . 'datetimepicker.class.php';
        $layout = new Widget_FlowLayout();

        parent::__construct($id, $layout);

        if($withTime){
            $this->from 	= new Widget_DateTimePicker($this->getId().'_from');
            $this->to 		= new Widget_DateTimePicker($this->getId().'_to');
        }else{
            $this->from 	= new Widget_DatePicker($this->getId().'_from');
            $this->to 		= new Widget_DatePicker($this->getId().'_to');
        }

        $this->from->setName('from')->setMetadata('to', $this->to->getId());
        $this->to->setName('to')->setMetadata('from', $this->from->getId());

        $this->setFormat('%d-%m-%Y');


        $W = bab_Widgets();


        $layout
        //	->setHorizontalSpacing(20)

            ->addItem(
                $W->FlowLayout()
                    ->addItem($this->fromLabel = $W->Label(widget_translate('From')))
                    ->addItem($this->from->setAssociatedLabel($this->fromLabel)
                )->setVerticalAlign('middle')->setHorizontalSpacing(.3,'em')
            )

            ->addItem(
                $W->FlowLayout()->addItem($this->toLabel = $W->Label(widget_translate('To')))
                    ->addItem($this->to->setAssociatedLabel($this->toLabel)
                )->setVerticalAlign('middle')->setHorizontalSpacing(.3,'em')
            );
    }


    /**
     * Sets the format of the date.
     * Only usage of %d, %m, %Y are allowed
     *
     * @param string $format	The format string as described in the php strftime function.
     * @return self
     */
    public function setFormat($format)
    {
        $this->from->setFormat($format);
        $this->to->setFormat($format);
        return $this;
    }

    /**
     * Set the date to highlight on first opening if the field is blank.
     * @param string $date YYYY-MM-DD
     * @return Widget_DatePicker
     */
    public function setDefaultDate($date)
    {
        $this->from->setDefaultDate($date);
        $this->to->setDefaultDate($date);
        return $this;
    }


    /**
     * Set the names on date fields for the start date and the end date of the period
     * @return self
     */
    public function setNames($from_name, $to_name) {
        $this->from->setName($from_name);
        $this->to->setName($to_name);
        return $this;
    }


    /**
     * Set the labels text for date fields for the start date and the end date of the period
     * @return self
     */
    public function setLabels($from_label, $to_label) {
        $this->fromLabel->setText($from_label);
        $this->toLabel->setText($to_label);
        return $this;
    }


    /**
     * Set the labels text for date fields for the start date and the end date of the period
     * @return self
     */
    public function setPlaceHolders($from_placeholder, $to_placeholder) {
        $this->from->setPlaceHolder($from_placeholder);
        $this->to->setPlaceHolder($to_placeholder);
        return $this;
    }


    /**
     * Set the size for date fields for the start date and the end date of the period
     * @param int $size
     * @return self
     */
    public function setSize($size) {
        $this->from->setSize($size);
        $this->to->setSize($size);
        return $this;
    }


    /**
     * Add class to sub input
     * @param string|array $className,... One or more class names.
     * @return self
     */
    public function addInputClass($class /*,... */) {
        $this->from->addClass($class);
        $this->to->addClass($class);
        return $this;
    }


    /**
     * Change the number of months displayed by the period picker
     *
     * @param int $numberOfMonthsHorizontal
     * @param int $numberOfMonthsVertical
     * @return self
     */
    public function setNumberOfMonths($numberOfMonthsHorizontal, $numberOfMonthsVertical = 1)
    {
        $this->from->setNumberOfMonths($numberOfMonthsHorizontal, $numberOfMonthsVertical);
        $this->to->setNumberOfMonths($numberOfMonthsHorizontal, $numberOfMonthsVertical);
        return $this;
    }


    /**
     * Returns the number of months displayed by the period picker.
     *
     * @return int
     */
    public function getNumberOfMonths()
    {
        return $this->from->getNumberOfMonths();
    }


	/**
	 * Activates or deactivates the possibility for the user to choose the
	 * year from a drop down list.
	 *
	 * @param bool $active
     * @return self
     */
    public function setYearSelectable($active = true)
    {
        $this->from->setYearSelectable($active);
        $this->to->setYearSelectable($active);
        return $this;
    }


    /**
     * Returns true if the user has the possibility to choose the
     * year from a drop down list.
     *
     * @return bool
     */
    public function isYearSelectable()
    {
        return $this->from->isYearSelectable();
    }


    /**
     * Activates or deactivates the possibility for the user to choose the
     * month from a drop down list.
     *
     * @param bool $active
     * @return self
     */
    public function setMonthSelectable($active = true)
    {
        $this->from->setMonthSelectable($active);
        $this->to->setMonthSelectable($active);
        return $this;
    }

    /**
     * Returns true if the user has the possibility to choose the
     * month from a drop down list.
     *
     * @return bool
     */
    public function isMonthSelectable()
    {
        return $this->from->isMonthSelectable();
    }



    /**
     * Display or hide week numbers on the datepicker.
     *
     * @param bool $show
     *            True/false to display/hide week numbers on datepicker.
     *            If not specified, showWeek returns the current value.
     * @return self|bool
     */
    public function showWeek($show = null)
    {
        if (isset($show)) {
            $this->from->showWeek($show);
            $this->to->showWeek($show);
            return $this;
        }
        return $this->from->showWeek();
    }

    /**
     * Set the labels text for date fields for the start date and the end date of the period
     * @return self
     */
    public function setValues($from_value, $to_value)
    {
        $this->from->setValue($from_value);
        $this->to->setValue($to_value);
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-periodpicker';
        return $classes;
    }

    public function isMandatory()
    {
        if ( $this->from->isMandatory() || $this->to->isMandatory() ){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Sets whether the field is mandatory or not.
     *
     * If the field is inside a dialog, the dialog will report an error to
     * the user if he did not fill in the field on submit.
     * This method returns the widget itself so that other methods can be chained.
     *
     * @param boolean $mandatory
     * @param string $message		The message displayed by the dialog if the field is left empty.
     * @return self
     */
    public function setMandatory($mandatory = true, $message = '')
    {
        $this->from->setMandatory($mandatory, $message = '');
        $this->to->setMandatory($mandatory, $message = '');
        return $this;
    }

    /**
     * Activates or deactivates the possibility for the user to choose the
     * month from a drop down list.
     *
     * @param bool $active
     * @return self
     */
    public function setSelectableMonth($active = true) {
        $this->from->setSelectableMonth($active);
        $this->to->setSelectableMonth($active);
        return $this;
    }

    /**
     * Activates or deactivates the possibility for the user to choose the
     * year from a drop down list.
     *
     * @param bool $active
     * @return self
     */
    public function setSelectableYear($active = true) {
        $this->from->setSelectableYear($active);
        $this->to->setSelectableYear($active);
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Item::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->fromLabel->colon(false);
        $this->toLabel->colon(false);
        return $canvas->div(
            $this->getId(),
            $this->getClasses(),
            $this->getLayout(),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        ) . $canvas->metadata($this->getId(), $this->getMetadata());
    }
}
