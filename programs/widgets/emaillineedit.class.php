<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/lineedit.class.php';


/**
 * Constructs a Widget_EmailLineEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_EmailLineEdit
 */
function Widget_EmailLineEdit($id = null)
{
	return new Widget_EmailLineEdit($id);
}


/**
 * A Widget_LineEdit is a widget that lets the user enter an email address.
 */
class Widget_EmailLineEdit extends Widget_LineEdit implements Widget_Displayable_Interface
{


	/**
	 * @param string $id			The item unique id.
	 * @return Widget_LineEdit
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);

		$this->setSubmitMessage(widget_translate('Invalid email address'));
	}

	/**
	 * Message displayed on form submit if there is a email line edit widget with an invalid email address
	 * @return Widget_EmailLineEdit
	 */
	public function setSubmitMessage($str)
	{
		$this->setMetadata('submitMessage', $str);
		return $this;
	}


	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-emaillineedit';
		return $classes;
	}
	
	
	protected function getInputType()
	{
	    return 'email';
	}


	public function display(Widget_Canvas $canvas)
	{
		if (!$this->isMandatory())
		{
			$message = $this->getMetadata('submitMessage');
			$message .= " \n".widget_translate('Do you want to submit the form anyway?');
			$this->setSubmitMessage($message);
		}

		$widgetsAddon = bab_getAddonInfosInstance('widgets');

		$output = parent::display($canvas);
		$output .= $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.emaillineedit.jquery.js');

		return $output;
	}
}
