<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/containerwidget.class.php';




function Widget_Balloon($id = null, $layout = null)
{
    return new Widget_Balloon($id, $layout);
}



class Widget_Balloon extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

    private $trigger_widget = null;


    public function __construct($id = null, $layout = null)
    {
        if (null === $layout) {
            require_once FUNC_WIDGETS_PHP_PATH . 'layout.class.php';
            $layout = new Widget_Layout();
        }


        parent::__construct($id, $layout);
    }


    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_Item#getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-balloon';

        return $classes;
    }


    /**
     * Display the balloon if mouse is over the item given in parameter
     * @param Widget_Displayable_Interface $item
     */
    public function displayOnMouseHover(Widget_Displayable_Interface $item)
    {
        $this->trigger_widget = $item;
        $this->setMetadata(__FUNCTION__, $item->getId());

        $item->addClass('widget-add-balloon');
        $item->setMetadata('_balloon', $this->getId());

        return $this;
    }



    /**
     * possibles positions are
     *
     *
     *  top left
     *  top
     *  top right
     *  left
     *  null
     *  right
     *  bottom left
     *  bottom
     *  bottom right
     *
     */
    public function setPosition($position)
    {
        $this->setMetadata('position', $position);
        return $this;
    }





    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_Displayable_Interface#display()
     */
    public function display(Widget_Canvas $canvas)
    {
        // convert balloon content to metadata, if empty use title attribute of the associated widget

        $layout = $this->getLayout();
        if (0 !== count($layout->getItems())) {
            $this->setMetadata('contents', $canvas->div(
                    null,
                    $this->getClasses(),
                    array($layout),
                    $this->getCanvasOptions(),
                    $this->getTitle(),
                    $this->getAttributes()
                )
            );
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->div(
            $this->getId(),
            null,
            array('')
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.balloon.jquery.js');
    }
}
