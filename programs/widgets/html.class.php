<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/widget.class.php';



/**
 * Constructs a Widget_Html.
 *
 * @param string    $html   The html content.
 * @param string    $id     The item unique id.
 * @return Widget_Html
 */
function Widget_Html($html = '', $id = null)
{
    return new Widget_Html($html, $id);
}



class Widget_Html extends Widget_Widget implements Widget_Displayable_Interface
{
    /**
     * @var string
     */
    private	$html;

    /**
    * Constructs a Widget_Html.
    *
    * @param string     $html   The html content.
    * @param string     $id	    The item unique id.
    */
    public function __construct($html, $id = null)
    {
        parent::__construct($id);
        $this->html = $html;
    }



    /**
     * @param string     $html   The html content.
     * @return self
     */
    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }


    /**
     * @return string   The html content.
     */
    public function getHtml()
    {
        return $this->html;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-html';
        return $classes;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Item::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        return $canvas->html(
            $this->getId(),
            $this->getClasses(),
            $this->html,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        ) . $canvas->metadata($this->getId(), $this->getMetadata());
    }
}
