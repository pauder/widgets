<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/frame.class.php';

/**
 *
 */
abstract class Widget_MultilangEdit extends Widget_Frame implements Widget_Displayable_Interface, Widget_AssociableLabel_Interface
{

    /**
     * @var array   The languages managed by this widget. Eg: array('en', 'fr', de').
     */
    protected $languages;

    protected $value = null;

    protected $widgetEdits = array();

    protected $_mandatory;

    protected $_mandatoryMessage;

    protected $_associatedLabel = null;


    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->languages = array();
    }



    /**
     * Sets the languages available in the multilang line edit.
     *
     * @param array $languages  The languages managed by this widget. Eg: array('en', 'fr', de').
     */
    public function setLanguages(array $languages)
    {
        $this->languages = $languages;

        $this->widgetEdits = array();
        foreach ($languages as $language) {
            $this->widgetEdits[$language] = $this->langWidgetEdit($language);
        }
        return $this;
    }



    /**
     * Returns the languages available in the multilang line edit.
     *
     * @return array
     */
    public function getLanguages()
    {
        return $this->languages;
    }



    /**
     * @param array $value  An array indexed by languages. Eg: array('en' => 'Hello', 'fr' => 'Bonjour).
     * @return Widget_MultilangLineEdit
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }


    /**
     * @return array
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_LineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-multilangedit';

        if ($this->_mandatory) {
            $classes[] = 'widget-input-mandatory';
        }
        return $classes;
    }



    /**
     *
     * @param string $lang
     * @return Widget_LineEdit
     */
    abstract protected function langWidgetEdit($lang);



    /**
     * Return the buttins used to switch between languages.
     * @return Widget_Layout
     */
    private function langSwitcher()
    {
        $W = bab_Widgets();
        $languages = $this->getLanguages();
        $langSwitcher = $W->FlowItems()->setSizePolicy('widget-lang-switcher');
        $selected = 'selected';
        foreach ($languages as $language) {
            $langSwitcher->addItem(
                    $W->Label($language)
                    ->addAttribute('lang', $language)
                    ->setMetadata('lang', $language)
                    ->addClass('widget-switch-lang', 'widget-switch-lang-' . $language, $selected)
            );
            $selected = '';
        }
        return $langSwitcher;
    }


    /**
     * Checks if the field has been set as mandatory.
     *
     * @return bool
     */
    public function isMandatory()
    {
        return $this->_mandatory;
    }


    /**
     * Sets whether the field is mandatory or not.
     *
     * If the field is inside a dialog, the dialog will report an error to
     * the user if he did not fill in the field on submit.
     * This method returns the widget itself so that other methods can be chained.
     *
     * @param boolean $mandatory
     * @param string $message		The message displayed by the dialog if the field is left empty.
     * @return Widget_Widget
     */
    public function setMandatory($mandatory = true, $message = '')
    {
        assert('is_bool($mandatory); /* The "mandatory" parameter must be a boolean. */');
        assert('is_string($message); /* The "message" parameter must be a string. */');
        $this->_mandatory = $mandatory;
        $this->_mandatoryMessage = $message;

        if ($this->_mandatory) {
            $this->setMetadata('mandatoryErrorMessage', $message);
        }

        return $this;
    }



    /**
     * @param Widget_Canvas	$canvas		The canvas
     * @return	string		The rendered string corresponding to the object on the specified canvas.
     */
    public function setAssociatedLabel(Widget_Label $widget = null)
    {
        $this->_associatedLabel = $widget;
        if ($this !== $widget->getAssociatedWidget()) {
            $widget->setAssociatedWidget($this);
        }
        return $this;
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Frame::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = bab_Widgets();
        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        $value = $this->getValue();
        $layout = $W->VBoxItems();
        foreach ($this->widgetEdits as $language => $widgetEdit) {
            if (isset($value[$language])) {
                $widgetEdit->setValue($value[$language]);
            }
            $layout->addItem($widgetEdit);
        }
        $layout->addItem($this->langSwitcher());

        parent::setLayout($layout);

        $display = parent::display($canvas);
        $display .= $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.multilang.jquery.js');
        return $display;
    }
}