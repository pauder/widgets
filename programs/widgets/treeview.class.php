<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/treeviewnodelayout.class.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/tree.php';


/**
 * Constructs a Widget_TreeView.
 *
 * @param Widget_Layout $layout     The layout that will manage how widgets are displayed in this container.
 * @param string        $id         The item unique id.
 * @return Widget_TreeView
 */
function Widget_TreeView($id = null)
{
    return new Widget_TreeView($id);
}



/**
 * Widget_TreeViewNode
 */
class Widget_TreeViewNode extends Widget_ContainerWidget implements Widget_Displayable_Interface
{
    private $childNodes = array();


    /**
     * @param string    $id     The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }


    /**
     * Appends $node as the last child of the node.
     *
     * @param Widget_TreeViewNode   $node       An element created by the method createElement.
     * @return Widget_TreeViewNode
     */
    public function appendChild(Widget_TreeViewNode $node)
    {
        array_push($this->childNodes, $node);
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see addons/widgets/widgets/Widget_ContainerWidget#setLayout()
     */
    public function setLayout(Widget_TreeViewNodeLayout $layout)
    {
        
    }

    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-treeviewnode';
        return $classes;
    }


    public function display(Widget_Canvas $canvas)
    {
        $this->tree->setCanvas($canvas);
        return $canvas->treeviewnode(
            $this->getId(),
            $this->getClasses(),
            $this->tree
        );
    }
}


/**
 * Widget_TreeView
 */
class Widget_TreeView extends Widget_TreeViewNode
{
    private $allNodes = array();



    /**
     * @param string $id            The item unique id.
     * @return Widget_TreeView
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }


    /**
     *
     * @return Widget_TreeViewNode
     */
    public function createNode($id)
    {
        $node = new Widget_TreeViewNode($id);
        array_push($this->allNodes, $node);
        return $node;
    }




    /**
     *
     * @param bool  $visible        True to have it visible.
     * @return unknown_type
     */
    public function visibleToolbar($visible)
    {
        $this->visibleToolbar = $visible;
        return $this;
    }

    /**
     * Shows the toolbar with search / expand and collapse.
     *
     * @return Widget_TreeView
     */
    public function showToolbar()
    {
        $this->visibleToolbar(true);
        return $this;
    }


    /**
     * Hides the toolbar with search / expand and collapse.
     *
     * @return Widget_TreeView
     */
    public function hideToolbar()
    {
        $this->visibleToolbar(false);
        return $this;
    }

    /**
     * Highlight the specified element in the treeview.
     *
     * @param string    $id     The treeview element id used in createElement.
     * @return Widget_TreeView
     */
    public function highlightElement($id)
    {
        $this->tree->highlightElement($id);
        return $this;
    }


    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-treeview';
        return $classes;
    }


    public function display(Widget_Canvas $canvas)
    {
        $this->tree->setCanvas($canvas);
        return $canvas->simpletreeview(
            $this->getId(),
            $this->getClasses(),
            $this->tree
        );
    }
}
