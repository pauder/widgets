<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/textedit.class.php';




/**
 * A Widget_SimpleHtmlEdit is a widget that let the user enter multiple lines of text with some style formatting.
 */
class Widget_SimpleHtmlEdit extends Widget_TextEdit implements Widget_Displayable_Interface
{

    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }


    /**
     * (non-PHPdoc)
     * @see Widget_TextEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();

        $classes[] = 'widget-simplehtmledit';
        return $classes;
    }



    /**
     * Display the create link button
     * @param bool $status
     */
    public function showCreateLinkControl($status = true)
    {
        $this->setMetadata('showCreateLinkControl', $status);
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_TextEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if ($this->getMetadata('css') === null) {
            $this->setMetadata('css', bab_getCssUrl());
        }
        if ($this->getMetadata('lang') === null) {
            $this->setMetadata('lang', bab_getLanguage());
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        $display = parent::display($canvas);
        $display .= $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.simplehtmledit.jquery.js');
        $display .= $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.simplehtmledit.css');

        return $display;
    }
}
