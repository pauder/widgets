<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_Menu.
 *
 * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
 * @param string		$id			The item unique id.
 * @return Widget_Menu
 */
function Widget_Menu($id = null, Widget_Layout $layout = null)
{
	return new Widget_Menu($id, $layout);
}



/**
 * A Widget_Menu.
 *
 */
class Widget_Menu extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

	/**
	 * The widget to which the menu is attached.
	 * @var Widget_Widget
	 */
	private $attachedWidget = null;


	/**
	 * The optional text in the menu button.
	 * @var string
	 */
	private $buttonLabel = null;

	/**
	 * Optional html class applied to the menu button.
	 * @var string
	 */
	private $buttonClass = null;

	/**
	 * @param Widget_Layout $layout	The layout that will manage how widgets are displayed in this container.
	 * @param string $id			The item unique id.
	 * @return Widget_Menu
	 */
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		parent::__construct($id, $layout);
	}


	/**
	 * Makes the menu contextual to the specified widget.
	 *
	 * @param Widget_Item $widget
	 * @return Widget_Menu
	 */
	function attachTo(Widget_Item $widget)
	{
		$this->setMetadata('_attachedWidget', $widget->getId());
		$this->attachedWidget = $widget;
		return $this;
	}



	/**
	 * Returns the widget attached to the menu.
	 *
	 * @return Widget_Widget
	 */
	function getAttachedWidget()
	{
		return $this->attachedWidget;
	}



	/**
	 * Instanciates a separator that can be added to the menu.
	 *
	 * @return Widget_Widget
	 */
	public static function Separator()
	{
		/* @var $W Func_Widgets */
		$W = bab_Widgets();
		$separator = $W->Label('')->addClass('widget-menu-separator');
		return $separator;
	}



	/**
	 * Instanciates an entry that can be added to the menu.
	 *
	 * @param string			$text	The label text.
	 * @param Widget_Action		$action	The action.
	 * @param string			$image	The image name.
	 * @param string			$id		The item unique id.
	 *
	 * @return Widget_Link
	 */
	public static function Entry($text, $action, $image = '', $id = null)
	{
		/* @var $W Func_Widgets */
		$W = bab_Widgets();
		$entry = $W->Link($text, $action, $id)->addClass('icon ' . $image);
		return $entry;
	}

	/**
	 * @param string			$text	The label text.
	 * @param Widget_Action		$action	The action.
	 * @param string			$image	The image name.
	 * @param string			$id		The item unique id.
	 *
	 * @return Widget_Menu
	 */
	public function addEntry($text, $action, $image = '', $id = null)
	{
		return $this->addItem(
			self::Entry($text, $action, $image, $id)
		);
	}


	/**
	 * @param Widget_Action		$action	The action.
	 * @param string			$id		The item unique id.
	 *
	 * @return Widget_Menu
	 */
	public function addAction($action, $id = null)
	{
		return $this->addItem(
			self::Entry($action->getTitle(), $action, $action->getIcon(), $id)
		);
	}


	/**
	 *
	 * @return Widget_Menu
	 */
	public function addSeparator()
	{
		return $this->addItem(
			self::Separator()
		);
	}



	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Item#getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-menu';
		return $classes;
	}



	/**
	 * Sets the text in the menu button.
	 * @since 1.0.62
	 * @param string $text
	 * @return $this
	 */
	public function setButtonLabel($text)
	{
	    $this->buttonLabel = $text;
	    return $this;
	}


	/**
	 * The text in the menu button.
	 * @since 1.0.62
	 * @return string|null
	 */
	public function getButtonLabel()
	{
	    return $this->buttonLabel;
	}



	/**
	 * Sets additional html class applied to the menu button.
	  @since 1.0.62
	 * @param string $text
	 * @return $this
	 */
	public function setButtonClass($text)
	{
	    $this->buttonClass = $text;
	    return $this;
	}


	/**
	 * The additional html class applied to the menu button.
	 * @since 1.0.62
	 * @return string|null
	 */
	public function getButtonClass()
	{
	    return $this->buttonClass;
	}

	/**
	 * Set a javascript function name to call when the menu is opened
	 *
	 * @param 	string 	$js_function
	 * @param	string	$domain
	 *
	 * @return Widget_Menu
	 */
	public function onOpen($js_function, $domain = 'window.babAddonWidgets')
	{
		$arr = $this->getMetadata('open');
		$arr[] = $domain.'.'.$js_function;

		$this->setMetadata('open', $arr);
		return $this;
	}



	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Displayable_Interface#display()
	 */
	public function display(Widget_Canvas $canvas)
	{
	    $buttonClass = $this->getButtonClass();
	    if (isset($buttonClass)) {
	        $this->setMetadata('buttonClass', $buttonClass);
	    }
		$buttonLabel = $this->getButtonLabel();
	    if (isset($buttonLabel)) {
	        $this->setMetadata('buttonLabel', $buttonLabel);
	    }

	    $widgetsAddon = bab_getAddonInfosInstance('widgets');

	    return $canvas->div(
		    $this->getId(),
			$this->getClasses(),
			array($this->getLayout()),
		    $this->getCanvasOptions(),
		    $this->getTitle(),
		    $this->getAttributes()
		)
		. $canvas->metadata($this->getId(), $this->getMetadata())
		. $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.menu.jquery.js');
	}

}

