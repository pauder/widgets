<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/frame.class.php';
require_once dirname(__FILE__) . '/vboxlayout.class.php';
require_once dirname(__FILE__) . '/multicolumnlayout.class.php';
require_once dirname(__FILE__) . '/hboxlayout.class.php';
require_once dirname(__FILE__) . '/label.class.php';
require_once dirname(__FILE__) . '/checkbox.class.php';


/**
 * @param string    $id     The item unique id.
 *
 * @return widget_CheckBoxModelView
 */
function widget_CheckBoxModelView($id = null)
{
	return new widget_CheckBoxModelView($id);
}




class widget_CheckBoxModelView extends Widget_Frame
{
    /**
     * @var ORM_Iterator	The data source.
     */
    protected $iterator = array();

    /**
     * @var string
     */
    private $namefield = 'name';

    /**
     * @var string
     */
    private $valuefield = 'id';

    /**
     * @var array
     */
    protected $height = null;

    /**
     * @var int
     */
    private $nbcolumn = 1;


    /**
     * Sets the data source.
     *
     * @param ORM_Iterator $iterator
     * @return self
     */
    public function setDataSource(ORM_Iterator $iterator)
    {
        $this->iterator = $iterator;
        return $this;
    }


    /**
     * Get data source
     * @return ORM_Iterator
     */
    public function getDataSource()
    {
        return $this->iterator;
    }


    /**
     *
     * @param string|ORM_Field $field
     * @return self
     */
    public function setNameField($field)
    {
        if ($field instanceof ORM_Field) {
            $field = $field->getName();
        }

        $this->namefield = $field;
        return $this;
    }


    /**
     * @param string|ORM_Field  $field
     *
     * @return self
     */
    public function setValueField($field)
    {
        if ($field instanceof ORM_Field) {
            $field = $field->getName();
        }

        $this->valuefield = $field;
        return $this;
    }


    /**
     * Defines the number of columns.
     *
     * @param   int $nb
     * @return self
     */
    public function setNbColumns($nb)
    {
        if ($nb > 0) {
            $this->nbcolumn = $nb;
        }
        return $this;
    }


    /**
     * Sets height of main frame.
     *
     * @param float $height
     * @param string $unit      Defaults to 'px'
     * @return self
     */
    public function setHeight($height, $unit = null)
    {
        $this->height = array($height, $unit);
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see Widget_Frame::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-checkboxmodelview';
        return $classes;
    }


    /**
     * {@inheritDoc}
     * @see Widget_Frame::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $layout = new Widget_MulticolumnLayout();
        $layout->setVerticalSpacing(.4, 'em');
        $layout->setHorizontalSpacing(3, 'em');
        $layout->setNbColumns($this->nbcolumn);

        foreach ($this->iterator as $record) {
            $checkbox = new Widget_CheckBox();
            $checkbox->setName($record->{$this->valuefield});

            $label = new Widget_Label($record->{$this->namefield});
            $label->setAssociatedWidget($checkbox);

            $hbox = new Widget_HBoxLayout();
            $hbox->addItem($checkbox);
            $hbox->addItem($label);
            $hbox->setHorizontalSpacing(.2,'em');
            $hbox->setVerticalAlign('middle');

            $layout->addItem($hbox);
        }

        $inner_frame = new Widget_Frame(null, $layout);

        if (isset($this->height)) {
            $options = $inner_frame->Options();
            $options->height($this->height[0], $this->height[1]);
            $inner_frame->setCanvasOptions($options);
        }

        $inner_frame->addClass('widget-checkboxmodelview-innerframe');

        if ($title = $this->getTitle()) {
            $label = new Widget_Label($title);
            $label->addClass('widget-checkboxmodelview-title');
            $this->addItem($label);
        }

        $this->addItem($inner_frame);

        return parent::display($canvas);
    }
}
