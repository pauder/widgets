<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/icon.class.php';



/**
 * Constructs a Widget_FileIcon.
 *
 * @param bab_Path		$path		Path to file
 * @param string		$id			The item unique id.
 * @return Widget_FileIcon
 */
function Widget_FileIcon($text, bab_Path $path, $id = null)
{
    return new Widget_FileIcon($text, $path, $id);
}


/**
 * Create a icon for a file
 * the file will have a default icon based on filetype and a thumbnail if possible
 */
class Widget_FileIcon extends Widget_Icon implements Widget_Displayable_Interface
{
    private $path;

    private $thumb_width = 48;
    private $thumb_height = 48;

    private $border;


    /**
    * Constructs a Widget_FileIcon.
    *
     * @param bab_Path		$path		Path to file
     * @param string		$id			The item unique id.
    * @return Widget_FileIcon
    */
    public function __construct($text, bab_Path $path, $id = null)
    {

        $this->path = $path;
        parent::__construct($text, Func_Icons::MIMETYPES_UNKNOWN);

        $this->border = array(1, '#aaaaaa');
    }


    /**
     *
     * @param int $size
     * @param string $color     hex color string prefixed with #
     * @return Widget_FileIcon
     */
    public function setBorder($size, $color)
    {
        $this->border = array($size, $color);
        return $this;
    }

    /**
     * Load thumbnail if exists or prepare needed metadata and session infos for javascript request
     * @return Widget_FileIcon
     */
    private function checkForThumbnail()
    {
        if (isset($this->path) && $T = @bab_functionality::get('Thumbnailer'))
        {
            $T->setSourceFile($this->path);
            $T->setImageFormat('png');

            if (!empty($this->border[0])) {
                $T->setBorder($this->border[0], $this->border[1]);
            }

            // if there is already a thumbnail, use it
            $url = $T->getCreatedThumbnail($this->thumb_width, $this->thumb_height);

            if (null !== $url && false !== $url)
            {
                $this->setImageUrl($url);
            } else {
                // use generic classname
                $this->setStockImage($T->getGenericClassName());

                // add metadata necessary for thumbnail creation

                $this->setMetadata('pending_thumbnail', 1);
                $this->setMetadata('selfpage', bab_getSelf());
                $this->setMetadata('controller', version_compare(bab_getDbVersion(), '7.8.90', '>=') ? 'addon=widgets.thumbnail' : 'tg=addon/widgets/thumbnail');

                // push in session the needed path

                $infos = array(
                    'path' => $this->path->toString(),
                    'width' => $this->thumb_width,
                    'height' => $this->thumb_height
                );

                if (!empty($this->border[0])) {
                    $infos['effects'] = array(
                        'setResizeMode' => array(Func_Thumbnailer::KEEP_ASPECT_RATIO),
                        'setImageFormat' => array('png'),
                        'setBorder' => $this->border
                    );
                }

                $_SESSION['addon_widgets']['Widget_FileIcon'][$this->getId()] = $infos;
            }
        }

        return $this;
    }


    public function setThumbnailSize($width, $height)
    {
        $this->thumb_width = $width;
        $this->thumb_height = $height;

        return $this;
    }



    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-fileicon';
        return $classes;
    }

    public function display(Widget_Canvas $canvas)
    {
        $this->checkForThumbnail();

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return parent::display($canvas)
            . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.fileicon-imagethumbnail.jquery.js');
    }
}
