<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/image.class.php';

bab_functionality::includeFile('Thumbnailer');

/**
 * Constructs a Widget_ImageThumbnail.
 *
 * @param bab_Path		$imageSrc	The image source (absolute path).
 * @param string		$labelText	The label text.
 * @param string		$id			The item unique id.
 * @return Widget_Image
 */
function Widget_ImageThumbnail(bab_Path $imageSrc, $labelText = '', $id = null)
{
    return new Widget_ImageThumbnail($imageSrc, $labelText, $id);
}



class Widget_ImageThumbnail extends Widget_Image implements Widget_Displayable_Interface
{
    private $path;

    private $thumb_width = 48;
    private $thumb_height = 48;

    private $resizemode = null;



    /**
     * @param string		$imageSrc	The image source (url).
     * @param string		$labelText	The 'alt' label text.
     * @param string		$id			The item unique id.
     */
    public function __construct(bab_Path $imageSrc, $labelText = '', $id = null)
    {
        $this->path = $imageSrc;

        parent::__construct('', $labelText, $id);
    }

    /**
     * {@inheritDoc}
     * @see Widget_Image::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-imagethumbnail';
        return $classes;
    }


    /**
     *
     * @param int $width
     * @param int $height
     */
    public function setThumbnailSize($width, $height)
    {
        $this->thumb_width = $width;
        $this->thumb_height = $height;

        return $this;
    }

    /**
     *
     * @param int $resizemode	Func_Thumbnailer::KEEP_ASPECT_RATIO | Func_Thumbnailer::CROP_CENTER
     */
    public function setResizeMode($resizemode)
    {
        $this->resizemode = $resizemode;
        return $this;
    }



    /**
     * Sets the default image that will be displayed if no thumbnail is available.
     *
     * @since 1.0.102
     * @param 	bab_Path $path
     * @return Widget_ImagePicker
     */
    public function setDefaultImage(bab_Path $path = null)
    {
        if (!isset($path)) {
            $this->defaultImage = null;
        } elseif ($T = bab_functionality::get('Thumbnailer')) {
            $T->setSourceFile($path->toString());
            $this->defaultImage = $T->getThumbnail($this->thumb_width, $this->thumb_height);
        }

        return $this;
    }


    /**
     * The url of the image displayed when no thumbnail is available.
     *
     * @since 1.0.102
     * @return string|null
     */
    protected function getDefaultImageUrl()
    {
        if (!isset($this->defaultImage)) {
            if ($T = bab_functionality::get('Thumbnailer')) {
               $addon = bab_getAddonInfosInstance('widgets');
               $T->setSourceFile($addon->getImagesPath().'photo.png');
               $this->defaultImage = $T->getThumbnail($this->thumb_width, $this->thumb_height);
            }
        }
        return $this->defaultImage;
    }


    /**
     * Load thumbnail if exists or prepare needed metadata and session infos for javascript request
     * @return Widget_FileIcon
     */
    private function checkForThumbnail()
    {
        if ($T = @bab_functionality::get('Thumbnailer')) {
            /*@var $T Func_Thumbnailer */
            $T->setSourceFile($this->path);
            $T->setImageFormat('png');

            if (null !== $this->resizemode) {
                $T->setResizeMode($this->resizemode);
            }

            // if there is already a thumbnail, use it
            $url = $T->getThumbnail($this->thumb_width, $this->thumb_height);
            //ALWAYS CREAT IT because it fails in ajax... ?

            if (null !== $url && false !== $url) {
                $this->setImageUrl($url);
            } else {
                $this->setImageUrl($this->getDefaultImageUrl());

                // add metadata necessary for thumbnail creation

                $this->setMetadata('pending_thumbnail', 1);
                $this->setMetadata('selfpage', bab_getSelf());
                $this->setMetadata('controller', 'addon=widgets.thumbnail');

                // push in session the needed path

                $arr = array(
                    'path' => $this->path->toString(),
                    'width' => $this->thumb_width,
                    'height' => $this->thumb_height,
                    'effects' => array(
                        'setResizeMode' => array(Func_Thumbnailer::KEEP_ASPECT_RATIO),
                        'setImageFormat' => array('png')
                    )
                );

                if (null !== $this->resizemode) {
                    $arr['effects']['setResizeMode'] = array($this->resizemode);
                }

                $_SESSION['addon_widgets']['Widget_FileIcon'][$this->getId()] = $arr;
            }
        }

        return $this;
    }



    public function createThumbnail()
    {
        $T = bab_functionality::get('Thumbnailer');
        /*@var $T Func_Thumbnailer */

        $T->setSourceFile($this->path);
        $T->setImageFormat('png');

        if ($this->resizemode) {
            $T->setResizeMode($this->resizemode);
        }

        $thumb = $T->getThumbnail($this->thumb_width, $this->thumb_height);

        return $thumb;
    }


    /**
     * @return string
     */
    public function getUrl()
    {
       return $this->createThumbnail();
    }


    /**
     * {@inheritDoc}
     * @see Widget_Image::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->checkForThumbnail();

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return parent::display($canvas)
          . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.fileicon-imagethumbnail.jquery.js');
    }
}
