<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/inputwidget.class.php';


/**
 * Constructs a Widget_Slider.
 *
 * @param string		$id			The item unique id.
 * @return Widget_Slider
 */
function Widget_Slider($id = null)
{
    return new Widget_Slider($id);
}


/**
 * A Widget_LineEdit is a widget that lets the user enter a single line of text.
 */
class Widget_Slider extends Widget_InputWidget implements Widget_Displayable_Interface
{
    /**
     * @var int
     */
    protected $_size;

    /**
     * @var int
     */
    protected $minvalue;
    protected $maxvalue;
    protected $step;


    /**
     * @var bool
     */
    protected $showButton;

    protected $inputClass = array();


    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->_size = null;
        $this->minvalue = 1;
        $this->maxvalue = 10;
        $this->step = 1;
        $this->showButton = false;
    }

    /**
     * Add the specified class names to the item.
     *
     * @param string|array $className,... One or more class names.
     * @return $this
     */
    public function addInputClass($className /*,... */)
    {
        $args = func_get_args();
        $numArgs = func_num_args();
        for ($i = 0; $i < $numArgs; $i++) {
            if (!is_array($args[$i])) {
                $args[$i] = array($args[$i]);
            }
            foreach ($args[$i] as $class) {
                $this->inputClass[] = $class;
            }
        }
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_InputWidget::getClasses()
     */
    public function getInputClasses()
    {
        $classes = $this->inputClass;
        if ($this->isPersistent() || $this->isReloadPersistent()) {
            $classes[] = 'widget-persistent';

            if (!$this->isReloadPersistent()) {
                $classes[] = 'widget-persistent-oninit';
            }

            if($this->persistent_storage == Widget_Widget::STORAGE_LOCAL) {
                $classes[] = 'widget-local-storage';
            } else if ($this->persistent_storage == Widget_Widget::STORAGE_SESSION) {
                $classes[] = 'widget-session-storage';
            } else if ($this->persistent_storage == Widget_Widget::STORAGE_COOKIES) {
                $classes[] = 'widget-cookies-storage';
            }
        }

        if (isset($this->ajaxUrl)) {
            $classes[] = 'widget-ajax';
        }
        if ($this->_mandatory) {
            $classes[] = 'widget-input-mandatory';
        }
        if ($this->_validateAjax) {
            $classes[] = 'widget-input-ajax-validate';
        }
        $classes[] = 'widget-slider-input';
        return $classes;
    }


    /**
     * Sets the visible size of the line edit .
     *
     * @param int $size
     * @return self
     */
    public function setSize($size)
    {
        assert('is_int($size); /* The "size" parameter must be an integer */');
        $this->_size = $size;
        return $this;
    }


    /**
     * Returns the visible size (in characters) of the line edit.
     *
     * @return int
     */
    public function getSize()
    {
        return $this->_size;
    }


    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-slider';
        return $classes;
    }

    /**
     *
     *
     * @param int $min
     * @return self
     */
    public function setMinValue($min)
    {
        $this->minvalue = $min;

        return $this;
    }

    /**
     *
     *
     * @return int
     */
    public function getMinValue()
    {
        return $this->minvalue;
    }

    /**
     *
     *
     * @param int $max
     * @return self
     */
    public function setMaxValue($max)
    {
        $this->maxvalue = $max;

        return $this;
    }

    /**
     *
     *
     * @return int
     */
    public function getMaxValue()
    {
        return $this->maxvalue;
    }

    /**
     *
     *
     * @param int $step
     * @return self
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     *
     *
     * @return int
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     *
     *
     * @param bool $show
     * @return self
     */
    public function setShowButton($show = true)
    {
        $this->showButton = $show;

        return $this;
    }

    /**
     *
     *
     * @return bool
     */
    public function getShowButton()
    {
        return $this->showButton;
    }



    /**
     * Initializes metadata.
     */
    protected function initMetadata()
    {
        $this->setMetadata('minValue', $this->getMinValue());
        $this->setMetadata('maxValue', $this->getMaxValue());
        $this->setMetadata('step', $this->getStep());
        $this->setMetadata('showButton', $this->getShowButton());
    }




    /**
     * @param Widget_Canvas $canvas
     *
     * @return string
     */
    protected function getScripts(Widget_Canvas $canvas)
    {
        $widgetAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->loadAddonScript($this->getId(), $widgetAddon, 'widgets.slider.jquery.js');
    }


    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $value = $this->getValue();

        $this->initMetadata();
        $metadata = $this->getMetadata();

        $classes = $this->getClasses();
        if ($this->isDisplayMode()) {
            $classes[] = 'widget-displaymode';
        }

        return $canvas->slider(
            $this->getId(),
            $classes,
            $this->getInputClasses(),
            $this->getFullName(),
            $value,
            $this->getSize(),
            $this->isDisabled(),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $metadata)
        . $this->getScripts($canvas);;
    }
}
