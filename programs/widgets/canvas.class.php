<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';





/**
 * The Canvas class provides primitives that must be used by widgets to generate their output.
 */
abstract class Widget_Canvas
{

	/**
	 * Returns a new Widget_CanvasOptions.
	 *
	 * @return Widget_CanvasOptions
	 */
	public static function Options()
	{
		require_once dirname(__FILE__).'/canvasoptions.class.php';
		return new Widget_CanvasOptions();
	}


	/**
	 * Returns a text element.
	 *
	 * @param string $text
	 * @return mixed
	 */
	abstract public function text($text);


	/**
	 * Returns a canvas label element.
	 *
	 * @param string				$id				The unique id (for the page).
	 * @param string[]				$classes		An array containing the classes as returned by Widget_Item::getClasses()
	 * @param string				$text
	 * @param string				$forId			The id of the element this label is for.
	 * @param Widget_CanvasOptions	$options
	 * @param string				$title			The (optional) tooltip
	 * @param string[]              $attributes     array of optional attributes
	 * @return mixed
	 */
	abstract public function label($id, $classes, $text, $forId, $options = null, $title = null, $attributes = null);


	/**
	 * Returns a canvas richtext element.
	 *
	 * @param string	$id					The unique id (for the page).
	 * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param string	$text
	 * @param int		$renderingOption	rendering options BAB_HTML_*.
	 * @param Widget_CanvasOptions	$options
	 * @return mixed
	 */
	abstract public function richtext($id, $classes, $text, $renderingOption, $options);


	/**
	 * Display some html.
	 *
	 * @param string	$id
	 * @param string[]	$classes
	 * @param string	$html
	 * @return string
	 */
	abstract public function html($id, $classes, $html, $options = null, $title = null, $attributes = null);


	/**
	 * Returns a canvas div element.
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
	 * @param mixed					$displayableItems		Array of Widget_Displayable_Interface objects or strings.
	 * @param Widget_CanvasOptions	$options				Options for layout items.
	 * @param string	            $title				    The tooltip title
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return mixed
	 */
	abstract public function div($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null);


	/**
	 * Returns a canvas header element.
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or strings
	 * @param int					$level					integer from 1 to 6, represent the importance of the header
	 * @param Widget_CanvasOptions	$options				Options for layout items.
	 * @param string	            $title				    The tooltip title
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return string
	 */
	abstract public function h($id, $classes, $displayableItems, $level, $options = null, $title = null, $attributes = null);


	/**
	 * Returns a canvas span element.
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects.
	 * @param Widget_CanvasOptions	$options				Options for layout items.
	 * @param string	            $title				    The tooltip title
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return string
	 */
	abstract public function span($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null);


	/**
	 * Display an image
	 * @param string				$id				The unique id (for the page).
	 * @param string[]				$classes		An array containing the classes as returned by Widget_Item::getClasses().
	 * @param string				$text			The alternate text if the image cannot be displayed.
	 * @param string				$url			The url of the image.
	 * @param Widget_CanvasOptions	$options
	 * @param string				$title			The optional tooltip
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return 	string
	 */
	abstract public function image($id, $classes, $text, $url, $options = null, $title = null, $attributes = null);

	/**
	 * Display a link.
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or strings.
	 * @param string				$url
	 * @param Widget_CanvasOptions	$options
	 * @param string				$title					The optional tooltip
	 * @param array					$attributes
	 * @return string
	 */
	abstract public function linkContainer($id, $classes, $displayableItems, $url, $options = null, $title = null, $attributes = null);


	/**
	 * Display a form
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses()
  	 * @param array					$fullName				An array containing the full name as returned by Widget_Item::getFullName(
	 * @param boolean				$readonly				Form generate a modification of data on server if true
	 * @param array					$hiddenFields			Hidden fields
	 * @param array					$displayableItems
	 * @param boolean				$selfPageHiddenFields	Add necessary hidden fields in form to submit on the same page
	 * @param string				$anchor					Set anchor in destination page
	 * @param Widget_CanvasOptions	$options				Canvas options
	 * @param string				$title					The optional tooltip
	 * @param array					$attributes
	 *
	 * @return mixed
	 * @abstract
	 */
	abstract public function form($id, $classes, $fullName, $readonly, $hiddenFields, $displayableItems, $selfPageHiddenFields, $anchor = null, $options = null, $title = null, $attributes = null);


	/**
	 * Returns an checkbox input field.
	 *
	 * @param string	$id					The unique id (for the page).
	 * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param array		$fullName			An array containing the full name as returned by Widget_Item::getFullName()
	 * @param bool		$value				The initial value.
	 * @param string	$checked_value
     * @param string	$unchecked_value	If not null, this value will be submitted if the checkbox is not checked. If null, no value is submitted if the checkbox is not checked.
	 * @param bool		$disabled
	 * @param string	$title				The tooltip title
	 * @param string[]  $attributes         Optional attributes
	 * @return mixed
	 */
	abstract public function checkBoxInput($id, $classes, $fullName, $value, $checked_value, $unchecked_value, $disabled, $title, $attributes = null);


	/**
	 * Returns a button
	 *
	 * @param string				$id					The unique id (for the page).
	 * @param string[]				$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param bool					$disabled
	 * @param array					$displayableItems	Array of Widget_Displayable_Interface objects.
	 * @param Widget_CanvasOptions	$options			Canvas options
	 * @param string				$title				button title
	 * @param array					$attributes         Optional attributes
	 * @return mixed
	 */
	abstract public function button($id, $classes, $disabled, $displayableItems, $options = null, $title = null, $attributes = null);


	/**
	 * Returns an HTML submit button
	 *
	 * @param string	$id					The unique (for the page) html id.
	 * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param array		$fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
	 * @param string	$label				button text.
	 * @param bool		$disabled
	 * @param string	$title				button title
	 * @param array     $attributes
	 * @return mixed
	 */
	abstract public function submitInput($id, $classes, $fullName, $label, $disabled, $title, $attributes = null);


	/**
	 * Returns an HTML reset button
	 *
	 * @param string	$id					The unique (for the page) html id.
	 * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param string	$label				button text.
	 * @param bool		$disabled
	 * @param string	$title				button title
	 * @param array     $attributes
	 * @return mixed
	 */
	abstract public function resetInput($id, $classes, $label, $disabled, $title, $attributes = null);


	/**
	 * Returns a hidden input field.
	 *
	 * @param string	$id					The unique (for the page) html id.
	 * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param array		$fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
	 * @param string	$value				The initial value.
	 * @param array     $attributes
	 * @return string
	 */
	abstract public function hidden($id, $classes, $fullName, $value, $attributes = null);


	/**
	 * Returns a single line text input field.
	 *
	 * @param string	            $id					The unique (for the page) html id.
	 * @param string[]	            $classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param array		            $fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
	 * @param string	            $value				The initial value.
	 * @param int		            $size				The size in character or NULL.
	 * @param int		            $maxSize			The maximum size in characters or NULL.
	 * @param bool		            $disabled
	 * @param bool		            $readOnly			The value of the readonly parameter.
	 * @param bool		            $autoComplete		The value of the autocomplete parameter.
	 * @param string	            $type				Html input type.
	 * @param string	            $placeholder		The placeholder text.
	 * @param Widget_CanvasOptions	$options			Options for layout items.
	 * @param string	            $title				The tooltip title
	 * @param string[]              $attributes         An array of name => value attributes.
	 * @return string
	 */
	abstract public function lineInput(
	    $id,
	    $classes,
	    $fullName,
	    $value,
	    $size,
	    $maxSize,
	    $disabled,
	    $readOnly = false,
	    $autoComplete = true,
	    $type = 'text',
	    $placeholder = null,
	    $options = null,
	    $title = null,
	    $attributes = null
	);

	/**
	 * Returns an jQuery Slider.
	 *
	 * @param string	            $id					The unique (for the page) html id.
	 * @param string[]	            $classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param string[]	            $inputClasses       An array containing the classes for the input item as returned by Widget_Slider::getInputClasses()
	 * @param array		            $fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
	 * @param string	            $value				The initial value.
	 * @param int		            $size				The size in character or NULL.
	 * @param bool		            $disabled
	 * @param Widget_CanvasOptions	$options			Options for layout items.
	 * @param string	            $title				The tooltip title
	 * @param string[]              $attributes         An array of name => value attributes.
	 * @return string
	 */
	abstract public function slider(
	    $id,
	    $classes,
	    $inputClasses,
	    $fullName,
	    $value,
	    $size,
	    $disabled,
	    $options = null,
	    $title = null,
	    $attributes = null
    );


	/**
	 * Returns a multi-line text input field.
	 *
	 * @param string	$id					The unique id (for the page).
	 * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param array		$fullName			An array containing the full name as returned by Widget_Item::getFullName()
	 * @param string	$value				The initial value.
	 * @param int		$width				The width in character or NULL.
	 * @param int		$height				The height in characters or NULL.
	 * @param bool		$disabled
	 * @param string    [$title]            Optional title
	 * @param array     [$attributes]       Optional attributes
	 * @return string
	 */
	abstract public function textInput($id, $classes, $fullName, $value, $width, $height, $disabled, $title = null, $attributes = null);


	/**
	 * Returns a file uploader.
	 *
	 * @param string	$id					The unique id (for the page).
	 * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array		$fullName			An array containing the full name as returned by Widget_Item::getFullName().
	 * @param array		$acceptedMimeType   The mime type.
	 * @param int		$size				The size of uploader.
	 * @param bool		$multiple			Multiple file upload, default false
	 * @param string    $title              Optional title
	 * @param array     $attributes         Optional attributes
	 * @return mixed
	 */
	abstract public function fileUpload($id, $classes, $fullName, $acceptedMimeType, $size, $multiple = false, $title = null, $attributes = null);

	/**
	 * Returns an HTML select.
	 *
	 * @param string				$id					The unique (for the page) html id.
	 * @param string[]				$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param array					$fullName			An array containing the full name as returned by Widget_Item::getFullName()
	 * @param mixed					$value				The initial value.
	 * @param array					$selectOptions		key values for select
	 * @param array					$optgroups			Options in groups
	 * @param bool					$disabled
	 * @param int					$size				Number of lines to display
	 * @param array					$classesOptions		Options classes
	 * @param array					$classesOptGroups	Options groups classes
	 * @param bool					$multiple			Multiple select
	 * @param Widget_CanvasOptions	$options			Options for layout items.
	 * @param string	            $title				The tooltip title
	 * @param string[]              $attributes         An array of name => value attributes.
	 * @return string
	 */
	abstract public function select(
	    $id,
	    $classes,
	    $fullName,
	    $value,
	    $selectOptions = null,
	    $optgroups = null,
	    $disabled = false,
	    $size = null,
	    $classesOptions = null,
	    $classesOptGroups = null,
	    $multiple = false,
	    $options = null,
	    $title = null,
	    $attributes = null
	);


	/**
	 * Return a default layout
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects.
	 * @param Widget_CanvasOptions	$options				Options for layout items
	 * @param string	            $title				    The tooltip title
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return string
	 */
	abstract public function defaultLayout($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null);


	/**
	 * Returns canvas flow layout.
	 *
	 * @param string				$id
	 * @param string[]				$classes
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
	 * @param Widget_CanvasOptions	$options				Options for layout items.
	 * @param string	            $title				    The tooltip title
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return string
	 */
	abstract public function flow($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null);


	/**
	 * Returns canvas hbox layout.
	 *
	 * @param string				$id
	 * @param string[]				$classes
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
	 * @param Widget_CanvasOptions	$options				Options for layout items.
	 * @param string	            $title				    The tooltip title
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return string
	 */
	abstract public function hbox($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null);


	/**
	 * Returns html for a vbox.
	 *
	 * @param string				$id
	 * @param string[]				$classes
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
	 * @param Widget_CanvasOptions	$options				Options for layout items.
	 * @param string	            $title				    The tooltip title
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return string
	 */
	abstract public function vbox($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null);



	/**
	 * Display a list of pair with a title
	 *
	 * @param string	$id					The unique (for the page) html id.
	 * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param string	$title				Title of list
	 * @param array		$displayableItems	Array of Widget_Displayable_Interface objects.
	 * @param string[]  $attributes         optional attributes
	 * @return string
	 */
	abstract public function PairVBox($id, $classes, $title, $displayableItems, $attributes = null);


	/**
	 * Returns canvas grid layout.
	 *
	 * @see Widget_Canvas::gridSection()
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array					$rows					An array of values returned by gridSection().
	 * @param array					$colsOptions			An array of Widget_CanvasOptions that will be applied to corresponding columns.
	 * @param Widget_CanvasOptions	$options				CanvasOptions that will be applied to the grid.
	 * @param string				$title				    The tooltip title.
	 * @param string[]				$attributes				Optional attributes.
	 * @return string
	 */
	abstract public function grid($id, $classes, $rows = array(), $colsOptions = array(), Widget_CanvasOptions $options = null, $title = null, $attributes = null);



	/**
	 * Returns a header grid section for the canvas grid layout.
	 *
	 * @see Widget_Canvas::gridRow()
	 *
	 * @param string                $id            The unique id (for the page).
	 * @param string[]              $classes       An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array                 $rows          Array of values returned by gridRow().
	 * @param Widget_CanvasOptions  $options       CanvasOptions that will be applied to the section.
	 */
	abstract public function gridHeaderSection($id, $classes, $rows = array(), $options = null);


	/**
	 * Returns a grid section for the canvas grid layout.
	 *
	 * @see Widget_Canvas::gridRow()
	 *
	 * @param string                $id            The unique id (for the page).
	 * @param string[]              $classes       An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array                 $rows          Array of values returned by gridRow().
	 * @param Widget_CanvasOptions  $options       CanvasOptions that will be applied to the section.
	 */
	abstract public function gridSection($id, $classes, $rows = array(), $options = null);

	/**
	 * Returns a grid row for the canvas grid layout.
	 *
	 * @see Widget_Canvas::gridRowCell()
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array					$cells					Array of values returned by gridRowCell().
	 * @param Widget_CanvasOptions	$options				CanvasOptions that will be applied to the row.
	 * @return string
	 */
	abstract public function gridRow($id, $classes, $cells = array(), $options = null);


	/**
	 * Returns a grid cell for grid row the canvas grid layout.
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
	 * @param Widget_CanvasOptions	$options				Options for layout items.
	 * @return string
	 */
	abstract public function gridRowCell($id, $classes, $displayableItems = array(), $options = null, $rowSpan = null, $colSpan = null, $colDesc = null);


	/**
	 * Display a menu with tab items
	 *
	 * @param string				$id						The unique id (for the page).
	 * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses()
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects.
	 * @param Widget_CanvasOptions	$options
	 *
	 * @return string
	 */
	abstract public function tabs($id, $classes, $displayableItems, $options);


    /**
     * Return a div which going to be a map.
     *
     * @param string                $id
     * @param string[]              $classes
     * @param int[string            $width                  Width (in px if no unit specified)
     * @param int|string            $height                 Height  (in px if no unit specified)
     * @param Widget_CanvasOptions  $options                Options for layout items
     * @param string                $title                  The tooltip title
     * @param string[]              $attributes             An array of name => value attributes
     * @return string
     **/
    abstract public function map($id, $classes, $width, $height, $options = null, $title = null, $attributes = null);


	/**
	 * Returns html for a unordered list.
	 *
	 * @param string				$id
	 * @param string[]				$classes
	 * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
	 * @param Widget_CanvasOptions	$options				Options for layout items
	 * @param string	            $title				    The tooltip title
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return string
	 */
	abstract public function ul($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null);

	/**
	 * Create anchor
	 * @param	string	$name
	 * @return string
	 */
	abstract public function anchor($name);


	/**
	 * Create a treeview node.
	 *
	 * @param string	           $id					The unique id (for the page).
	 * @param string[]	           $classes				An array containing the classes as returned by Widget_Item::getClasses()
	 * @param array		           $displayableItems	Array of Widget_Displayable_Interface objects.
	 * @param array		           $childNodes			An array of Widget_TreeViewNode
	 * @param Widget_CanvasOptions $options
	 * @return string
	 */
	abstract public function treeviewnode($id, Array $classes, Array $displayableItems, Array $childNodes, Widget_CanvasOptions $options = null);


	/**
	 * Create metadata container.
	 *
	 * @param	string	$name
	 * @param	mixed	$metadata
	 * @return string
	 */
	abstract public function metadata($name, $metadata = null);

	/**
	 * Load script
	 * @param	string	$widget_id
	 * @param	string	$filename
	 * @return string
	 */
	abstract public function loadScript($widget_id, $filename);


	/**
	 * Includes the specified script from the template path ot the specified addon.
	 * The addon version is appended to the script url to ensure that cached script will be
	 * reloaded on addon upgrade.
	 *
	 * @since 1.0.91
	 *
	 * @param string $widgetId
	 * @param bab_addonInfos $addon
	 * @param string $filename
	 * @return string
	 */
	abstract public function loadAddonScript($widgetId, bab_addonInfos $addon, $filename);


	/**
	 * Load CSS style sheet
	 * @param	string	$filename
	 * @return string
	 */
	abstract public function loadStyleSheet($filename);

	/**
	 * Includes the specified CSS style sheet from the style path of the specified addon.
	 * The addon version is appended to the css url to ensure that cached css will be
	 * reloaded on addon upgrade.
	 *
	 * @since 1.0.91
	 *
	 * @param bab_addonInfos $addon
	 * @param string $filename
	 * @return string
	 */
	abstract public function loadAddonStyleSheet(bab_addonInfos $addon, $filename);

	/**
	 * Returns an iframe element.
	 *
	 * @param string				$id			The unique id (for the page).
	 * @param string				$url		url of page to load in iframe
	 * @param Widget_CanvasOptions	$options
	 * @return mixed
	 */
	abstract public function iframe($id, $url, $options);


	/**
	 * Add optional decorations on page
	 * @param	Widget_Page	$page
	 * @param	string[]	$classes	class names used in widgets of the page
	 */
	abstract public function setPageDecorations(Widget_Page $page, $classes);
}
