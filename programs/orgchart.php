<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';


function widgets_getFunctionSubTree($ocid, $entity = null, $user_number = 5)
{
    static $orgUtil = null;
	static $info = null;
    if($orgUtil === null){
        $orgUtil = new bab_OrgChartUtil($ocid);
		$info = bab_OCGetOrgChart($ocid);
    }

    $arr = bab_OCGetDirectChildren($entity);

    if (empty($arr)) {
        return '';
    }
    $str = '<ul>';
    foreach ($arr as $identity => $infos) {
        $str .= sprintf('<li><a href="#">%s</a>', bab_toHtml($infos['name']));
        $str .= widgets_getFunctionSubTree($ocid, $identity, $user_number);
        $str .= "</li>\n";
    }

    $arr = $orgUtil->getRoleByEntityId($entity);
    foreach ($arr as $idrole => $infos) {
        if($infos['type'] != BAB_OC_ROLE_TEMPORARY_EMPLOYEE){
            $users = $orgUtil->getUsersRole($idrole);
            if(!empty($users)){
                $name = '';
                if(count($users)== 1){
                    $icon = '<span class="icon apps-users">';
                }else{
                    $icon = '<span class="icon apps-groups">';
                }
                if(count($users) > $user_number){
                    $name.= sprintf(widget_translate('More than %s user(s)'), $user_number);
                }else{
                    foreach($users as $user){
						$dirinfo = bab_getDirEntry($user, BAB_DIR_ENTRY_ID/*, $info['id_directory']*/);
						if (!$dirinfo) {
							continue;
						}
                        if($name != ''){
                            $name.= ', ';
                        }
                        $name.= $dirinfo['sn']['value'] . ' ' . $dirinfo['givenname']['value'];
                    }
                }
                $str .= sprintf('<li><a href="#" title="%s">'.$icon.'%s</span></a>', $idrole, bab_toHtml($infos['name'] . ' (' . $name . ')'));
                $str .= "</li>\n";
            }
        }
    }

    $str .= '</ul>'."\n";

    return $str;
}



function widgets_getFunctionTree($ocid, $relative, $entity = null, $user_number = 5)
{
    $orgUtil = new bab_OrgChartUtil($ocid);

    if($entity === null){
        $root = $orgUtil->getRoot();
        $entity = $root['id_entity'];
    }

    $entityName = bab_OCGetEntity($entity);

    $str = '<ul class="icon-left-16 icon-left icon-16x16">';
    $str .= sprintf('<li><a href="#">%s</a>', bab_toHtml($entityName['name']));

    $str .= widgets_getFunctionSubTree($ocid, $entity, $user_number);

    if($relative){
        $str .= '<li><a href="#" title="0"><span class="icon objects-manager">'.bab_translate("Immediat superior")."</span></a></li>\n";
        $str .= '<li><a href="#" title="-1"><span class="icon objects-manager">'.sprintf(bab_translate("Level %d superior"), 2)."</span></a></li>\n";
        $str .= '<li><a href="#" title="-2"><span class="icon objects-manager">'.sprintf(bab_translate("Level %d superior"), 3)."</span></a></li>\n";
    }
    //	$str .= '<li class="none"><a href="#" title="">'.widget_translate('None')."</a></li>\n";

    $str .= "</li>\n";


    $str .= '</ul>'."\n";

    return $str;
}



switch (bab_rp('idx'))
{

    case 'functionpicker':
        echo widgets_getFunctionTree(bab_gp('ocid', 1), bab_gp('relative', false), null, bab_gp('user_number', 5));
        break;
}

die;
