
function widgets_checkboxAllInit(domNode)
{
	jQuery(domNode).find('.widget-checkboxall').not('.widget-init-done').each(function () {

		var checkboxAll = jQuery(this);
		checkboxAll.addClass('widget-init-done');

		var group = window.babAddonWidgets.getMetadata(this.id).group;

		checkboxAll.change(function() {
			for(var i=0; i<group.length; i++) {
				jQuery('#'+group[i]).prop('checked', checkboxAll.prop('checked'));
			}
		});
		
	});
}


window.bab.addInitFunction(widgets_checkboxAllInit);
