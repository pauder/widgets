


function widgets_sectionInit(domNode)
{
    jQuery(domNode).find('.widget-section .widget-section-header.widget-foldable .handle').not('.widget-init-done').each(function () {

        jQuery(this).addClass('widget-init-done');

        jQuery(this).click(function() {
            var section = jQuery(this).closest('.widget-section');
            section.toggleClass('widget-folded');

            if (section.hasClass('widget-persistent')) {
                var expiryDate = new Date;
                expiryDate.setMonth(expiryDate.getMonth() + 6);
                var cookiePath = '/';

                var cookieString = section.hasClass('widget-folded') ? '1' : '0';

                //console.debug(cookieString);
                document.cookie = 'widgets_Section_' + section.attr('id') + '=' + escape(cookieString)
                                    + '; expires=' + expiryDate.toGMTString()
                                    + '; path=' + cookiePath;
                //console.debug(document.cookie);
            }
        });
    });
}


window.bab.addInitFunction(widgets_sectionInit);
