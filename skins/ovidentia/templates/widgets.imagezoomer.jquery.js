// need jquery 1.7.1.3 module with the lightbox jquery plugin


window.bab.addInitFunction(function() {
	
	if (typeof jQuery('.widget-imagezoomer').lightbox == 'function') {
		jQuery('.widget-imagezoomer').lightbox();
	} else {
	
		jQuery('.widget-imagezoomer').attr('data-lightbox', 'lightbox');
	}
});
