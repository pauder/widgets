window.babAddonWidgets.emailLineEditEvent = function() {

    var input = jQuery(this);

    if ('' == input.val())
    {
        input.addClass('widget-emaillineedit-valid');
        input.removeClass('widget-emaillineedit-invalid');
        return;
    }
    
    var reg = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    if(reg.test(input.val()) == false) {
        input.removeClass('widget-emaillineedit-valid');
        input.addClass('widget-emaillineedit-invalid');
    } else {
        input.addClass('widget-emaillineedit-valid');
        input.removeClass('widget-emaillineedit-invalid');
    }
};



function widget_emailLineEditInit(domNode) {

    jQuery(domNode).find('.widget-emaillineedit').not('.widget-init-done').each(function() {
        var input = jQuery(this);

        input.addClass('widget-init-done');

        if (input.data('widgetevent'))
        {
            return;
        }

        input.data('widgetevent', true);

        // lock form validation if not a valid email address

        input.blur(window.babAddonWidgets.emailLineEditEvent);
        input.keyup(window.babAddonWidgets.emailLineEditEvent);

        input.blur();


        // form event

        if (form = input.closest('form')) {
            form.on('validate.widgets', function() {
                if (input.hasClass('widget-emaillineedit-invalid')) {
                    input.focus();
                    var message = window.babAddonWidgets.getMetadata(input.attr('id')).submitMessage;

                    if (window.babAddonWidgets.validatemandatory) {
                        form.addClass('widget-invalid');
                        alert(message);
                        return false;
                    }

                    if (window.babAddonWidgets.validate) {
                        var confirm = window.confirm(message);
                        if (!confirm) {
                            //console.debug(input);
                            form.addClass('widget-invalid');
                        }
                        return confirm;
                    }
                }

                return true;
            });
        }

    });

}



window.bab.addInitFunction(widget_emailLineEditInit);
