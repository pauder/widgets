

function widget_getNodeId(li) {
	var v = li.attr('class');
	
	if (!v)
	{
		return null;
	}
	
	var c = v.split(' ');
	for(var i =0; i< c.length; i++)
	{
		if (c[i].indexOf('sitemap-ajax') == 0)
		{
			return c[i].substring(13);
		}
	}
	
	return null;
}



function widget_sitemapMenuInitUl(ul, depth, maxdepth, loading)
{
	
	ul.children('li').hover(function() {
		var li = jQuery(this);
		
		if (li.find('ul').length > 0)
		{
			widget_sitemapMenuInitUl(li.find('ul'), (depth +1), maxdepth, loading);
			return;
		}
		
		if (li.hasClass('loading'))
		{
			return;
		}
		
		var nodeId = widget_getNodeId(li);
		
		if (!nodeId)
		{
			return;
		}
		
		li.append(jQuery('<ul class="loading"><li><span class="icon status-content-loading">'+loading+'</span></li></ul>'));
		
		jQuery.get( '?addon=widgets.sitemapmenu&nodeId='+encodeURIComponent(nodeId)+'&maxdepth='+maxdepth+'&depth='+depth, function( data ) {
			var ul2 = jQuery(data);
			li.append(ul2);
			li.find('ul.loading').remove();
			widget_sitemapMenuInitUl(ul2, (depth +1), maxdepth, loading);
		});
		
	});
}



function widget_sitemapMenuInit(domNode) {
	
	jQuery(domNode).find('.widget-sitemap-menu').not('.widget-init-done').each(function() {
		var ul = jQuery(this);
		ul.addClass('widget-init-done');
		
		var meta = window.babAddonWidgets.getMetadata(ul.attr('id'));
		
		widget_sitemapMenuInitUl(ul, 1, meta.maxdepth, meta.loading);
	});
	
}



window.bab.addInitFunction(widget_sitemapMenuInit);
